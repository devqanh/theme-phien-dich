<?php
do_action('flatsome_before_blog');
?>

<?php if (!is_single() && flatsome_option('blog_featured') == 'top') {
    get_template_part('template-parts/posts/featured-posts');
} ?>
    <div class="row row-large <?php if (flatsome_option('blog_layout_divider')) echo 'row-divided '; ?>">
            <div class="post-sidebar large-3 col">
            <div id="text-8" class="widget widget_text">
                <div class="textwidget">
                    <div style="background-color:#C82254; color:#fff;padding:10px 15px; margin-bottom:20px;"> <strong>TP. HÀ NỘI </strong>
                        <br> <a href="tel:0975419415" title="Hãy gọi cho chúng tôi - 0975.419.415" id="hotline-1660161011" rel="nofollow" class="hotline " style="color:#fff;">0975.419.415</a><br> <a href="tel:0964333933" title="Hãy gọi cho chúng tôi - 0964.333.933" id="hotline-1586407023" rel="nofollow" class="hotline " style="color:#fff;">0964.333.933</a>
                        <div style="border-bottom:#fff dotted 1px; margin:10px 0;"></div> <strong>TP. HỒ CHÍ MINH </strong>
                        <br> <a href="tel:0964333933" title="Hãy gọi cho chúng tôi - 0964.333.933" id="hotline-1499312786" rel="nofollow" class="hotline " style="color:#fff;">0964.333.933</a><br> <a href="tel:0975419415" title="Hãy gọi cho chúng tôi - 0975.419.415" id="hotline-1293485500" rel="nofollow" class="hotline " style="color:#fff;">0975.419.415</a>
                        <div style="border-bottom:#fff dotted 1px; margin:10px 0;"></div><strong>CÁC TỈNH KHÁC</strong>
                        <br> <a href="tel:0975419415" title="Hãy gọi cho chúng tôi - 0975.419.415" id="hotline-1705452064" rel="nofollow" class="hotline " style="color:#fff;">0975.419.415</a><br> <a href="tel:0964333933" title="Hãy gọi cho chúng tôi - 0964.333.933" id="hotline-444306662" rel="nofollow" class="hotline " style="color:#fff;">0964.333.933</a></div>
                </div>
            </div>
        </div><!-- .post-sidebar -->
        <div class="large-9 col">
            <?php if (!is_single() && flatsome_option('blog_featured') == 'content') {
                get_template_part('template-parts/posts/featured-posts');
            } ?>
            <?php
            if (is_single()) {
                get_template_part('template-parts/posts/single');
                //comments_template();
            } elseif (flatsome_option('blog_style_archive') && (is_archive() || is_search())) {
                get_template_part('template-parts/posts/archive', flatsome_option('blog_style_archive'));
            } else {
                get_template_part('template-parts/posts/archive', flatsome_option('blog_style'));
            }
            ?>
        </div> <!-- .large-9 -->


    </div><!-- .row -->

<?php
do_action('flatsome_after_blog');
?>