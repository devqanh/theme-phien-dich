<?php
do_action('flatsome_before_blog');
?>

<?php if (!is_single() && flatsome_option('blog_featured') == 'top') {
    get_template_part('template-parts/posts/featured-posts');
} ?>
    <div class="row row-large <?php if (flatsome_option('blog_layout_divider')) echo 'row-divided '; ?>">
        <?php if (is_single()): ?>
            <div class="container title-bai-viet text-center">
                <h1 class="breadcrumbs0" style="color:#FFF;"><?php echo get_the_title(); ?></h1>
                <p id="breadcrumbs" class="text-center" style="color:#FFF;"><span><span><a
                                    href="https://dichthuatphuongdong.com/">Công ty dịch thuật</a> » <span><a
                                        href="https://dichthuatphuongdong.com/tin-tuc/">Tin tức</a> » <span
                                        class="breadcrumb_last"><?php echo get_the_title(); ?></span></span></span></span>
                </p>
            </div>
        <?php endif; ?>
        <div class="post-sidebar large-3 col">
            <?php get_sidebar(); ?>
        </div><!-- .post-sidebar -->
        <div class="large-9 col">
            <?php if (!is_single() && flatsome_option('blog_featured') == 'content') {
                get_template_part('template-parts/posts/featured-posts');
            } ?>
            <?php
            if (is_single()) {
                get_template_part('template-parts/posts/single');
                //comments_template();
            } elseif (flatsome_option('blog_style_archive') && (is_archive() || is_search())) {
                get_template_part('template-parts/posts/archive', flatsome_option('blog_style_archive'));
            } else {
                get_template_part('template-parts/posts/archive', flatsome_option('blog_style'));
            }
            ?>
        </div> <!-- .large-9 -->


    </div><!-- .row -->

<?php
do_action('flatsome_after_blog');
?>