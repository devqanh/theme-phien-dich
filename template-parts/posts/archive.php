<?php if (have_posts()) : ?>
    <div id="post-list">

        <?php /* Start the Loop */ ?>
        <?php while (have_posts()) : the_post(); ?>

            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <div class="article-inner <?php flatsome_blog_article_classes(); ?>">
                    <div class="list-item">
                        <div class="row">
                            <div class="col medium-4">
                                <a href="<?php echo get_the_permalink(); ?>">
                                    <div class="image">
                                    <?php the_post_thumbnail(); ?>
                                        <p></p>
                                        <div class="bg bg-loaded"></div>
                                        <div class="icon">+</div>
                                    </div>
                                    </a>
                            </div>
                            <div class="col medium-8">
                                <div class="desc">
                                    <h3>
                                        <a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a>
                                    </h3>
                                    <div class="meta"><span class="date"> <i
                                                    class="fa fa-calendar"></i><?php echo get_the_date(); ?></span> <span class="clock"> <i
                                                    class="fa fa-clock-o"></i><?php echo get_the_time(); ?></span></div>
                                    <p class="txt">
                                        <?php echo wp_trim_words(get_the_excerpt(), 10, "Trống"); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- .article-inner -->
            </article><!-- #-<?php the_ID(); ?> -->

        <?php endwhile; ?>

        <?php flatsome_posts_pagination(); ?>

    </div>

<?php else : ?>

    <?php get_template_part('template-parts/posts/content', 'none'); ?>

<?php endif; ?>