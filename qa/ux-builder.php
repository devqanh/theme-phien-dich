<?php
define('QA_PATCH', get_stylesheet_directory_uri());

function devqa_ux_builder_element()
{
    add_ux_builder_shortcode('devqa_viewnumber', array(
        'name' => __('Kiểu bài post hiển thị'),
        'category' => __('Content ITSWEB'),
        'thumbnail' => QA_PATCH . '/qa/assets/blog_posts.svg',
        'priority' => 1,
        'options' => array(
            'style' => array(
                'type' => 'radio-buttons',
                'heading' => __('Style'),
                'default' => 'outline',
                'options' => array(
                    'style-1' => array('title' => 'Kiểu Nổi bật'),
                    'style-2' => array('title' => 'Kiểu Tin tức'),
                ),
            ),
            'category' => array(
                'type' => 'select',
                'heading' => __('Chọn category', 'flatsome'),
                'full_width' => true,
                'config' => array(
                    'placeholder' => __('Select', 'flatsome'),
                    'termSelect' => array(
                        'post_type' => 'post',
                        'taxonomies' => 'category'
                    ),
                )
            ),
            'title_qa' => array(
                'type' => 'textfield',
                'heading' => 'Tiêu đề 1',
                'default' => '',
            ),
            'number_post' => array('type' => 'textfield', 'heading' => 'Số bài hiển thị', 'default' => '')

        )));
}

add_action('ux_builder_setup', 'devqa_ux_builder_element');

function devqa_viewnumber_func($atts)
{

    extract(shortcode_atts(array(
        'style' => '',
        'category' => '',
        'title_qa' => '',
        'number_post' => '1'
    ), $atts));
    $style = !empty($style) ? $style : '';
    $cat = !empty($category) ? $category : '';
    $title_qa = !empty($title_qa) ? $title_qa : '';
    $number_post = !empty($number_post) ? $number_post : '';
    ob_start();
    if ($style == 'style-1') :
        echo template_post_qa_1($title_qa, $cat, $number_post);
    elseif ($style == 'style-2') :
        echo template_post_qa_2($title_qa, $cat, $number_post);
    endif;
    return ob_get_clean();
}

add_shortcode('devqa_viewnumber', 'devqa_viewnumber_func');

function template_post_qa_1($title, $cat, $number_post)
{
    ob_start(); ?>
    <h2 id="adv-heading" class="advancedheading" style="text-align:center;color:#f84805"><?php echo $title ?></h2>
    <p style="text-align:center" class="has-medium-font-size">Một số dự án biên phiên dịch mới nhất mà đội ngũ nhân sự
        và<br/>các cộng tác viên của chúng tôi thực hiện gần đây.</p>
    <div class="row">
        <?php
        $tham_so = array(
            'cat' => $cat,
            'posts_per_page' => $number_post,

        );
        $the_query = new WP_Query($tham_so);
        if ($the_query->have_posts()) :
            while ($the_query->have_posts()) : $the_query->the_post(); ?>
                <div class="col medium-4">
                    <a href="<?php echo get_permalink();?>" class="item">
                        <div class="img">
                            <?php echo get_the_post_thumbnail();?>
                            <div class="bg"></div>
                            <div class="icon">+</div>
                        </div>
                        <div class="desc">
                            <h3><?php echo get_the_title();?></h3>
                        </div>
                        <p>
                    </a>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>

    </div>
    <?php
    return ob_get_clean();
}

function template_post_qa_2($title, $cat, $number_post)
{
    ob_start(); ?>

    <h2 id="adv-heading" class="adv-heading6 advancedheading" style="text-align:center;color:#f84805;"><?php echo $title;?></h2>
    <div class="row">
        <?php
        $tham_so = array(
            'cat' => $cat,
            'posts_per_page' => $number_post,

        );
        $the_query = new WP_Query($tham_so);
        if ($the_query->have_posts()) :
            while ($the_query->have_posts()) : $the_query->the_post(); ?>
                <div class="col medium-4">
                    <a href="<?php echo get_permalink();?>" class="item">
                        <?php echo get_the_post_thumbnail();?>
                        <div class="item-title"><?php echo get_the_title();?></div>
                    </a>
                </div>
        <?php endwhile; ?>
    <?php endif; ?>
    </div>

    </div>
    <?php
    return ob_get_clean();
}
